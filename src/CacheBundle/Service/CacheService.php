<?php

namespace CacheBundle\Service;
use CacheBundle\CacheEventListener\FilmCacheListener;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\ORM\EntityManager;

/**
 * Date: 19/03/16
 * Time: 21:31
 */
class CacheService
{
    const LIST_ALL_CACHE_ID = 'list_all_cache_id';
    const LIFETIME_CACHE    = 6;
    private $entityManager;
    private $apcCacheDriver;
    private $filmCacheListener;
    private $listResult;

    public function __construct(EntityManager $entityManager, FilmCacheListener $filmCacheListener)
    {
        $this->apcCacheDriver       = new ApcCache();
        $this->entityManager        = $entityManager;
        $this->filmCacheListener    = $filmCacheListener;
    }

    public function filmsListExistsOnCache()
    {
        if(self::checkCacheIdExists()){
            $this->listResult = $this->apcCacheDriver->fetch(self::LIST_ALL_CACHE_ID);
            $this->filmCacheListener->stopPropagation();
        }
    }

    public function filmsListSaveOnCache()
    {
        $films_repository = $this
            ->entityManager
            ->getRepository('FilmBundle:Film');

        $this->listResult = $films_repository->findAll();

//      Another method: Creating query from EntityManager:
//        $entityManagerQuery = $this->entityManager->createQuery('SELECT f FROM '. $films_repository->getClassName() .' f');
//        $this->listResult = $entityManagerQuery->getResult();

        $this->apcCacheDriver->save(self::LIST_ALL_CACHE_ID, $this->listResult, self::LIFETIME_CACHE);
    }

    private function checkCacheIdExists()
    {
        return $this->apcCacheDriver->contains(self::LIST_ALL_CACHE_ID);
    }

    public function returnListResult()
    {
        return $this->listResult;
    }
}