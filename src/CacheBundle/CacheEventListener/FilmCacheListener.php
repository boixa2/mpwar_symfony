<?php

namespace CacheBundle\CacheEventListener;
use CacheBundle\Service\CacheService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

/**
 * Date: 02/04/16
 * Time: 01:54
 */
class FilmCacheListener extends Event
{
    const LIST_ALL_FILMS_CACHE_NAME_EVENT           = 'films.listAllFromCache';
    const LIST_ALL_FILMS_EVENT_NAME                 = 'films.listAll';

    const FILMS_LIST_CACHE_EXISTS                   = 'filmsListExistsOnCache';
    const FILMS_LIST_SAVE_ON_CACHE                  = 'filmsListSaveOnCache';

    const LIST_ALL_CACHE_ID                         = 'list_all_cache_id';

    private $cacheService;
    private $eventDispatcher;

    public function __construct(EntityManager $entityManager,
                                EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        $this->cacheService     = new CacheService($entityManager, $this);
        $this->eventDispatcher  = $eventDispatcher;
    }

    public function dipatchFilmsListListeners()
    {
        self::filmListPrepareListeners();
        $this->eventDispatcher->dispatch(self::LIST_ALL_FILMS_EVENT_NAME, $this);
    }

    /*
     * @return FilmBundle/Entity/Film
     */
    public function getFilmsListResults()
    {
        return $this->cacheService->returnListResult();
    }

    private function filmListPrepareListeners()
    {
        $this->eventDispatcher->addListener(
            self::LIST_ALL_FILMS_EVENT_NAME,
            array($this->cacheService,
                self::FILMS_LIST_CACHE_EXISTS),
            1);
        $this->eventDispatcher->addListener(
            self::LIST_ALL_FILMS_EVENT_NAME,
            array(
                $this->cacheService,
                self::FILMS_LIST_SAVE_ON_CACHE),
            0);
    }

}