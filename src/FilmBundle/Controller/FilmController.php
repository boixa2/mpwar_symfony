<?php

namespace FilmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CacheBundle\Service\CacheService;

/**
 * Date: 08/03/16
 * Time: 21:01
 */
class FilmController extends Controller
{
    private $response;

    /**
     * @Route ("/Film/add/{name}/{year}/{date}/{url}", name="add_film")
     */
    public function newFilm($name, $year, $date, $url)
    {
        $film_service = $this->get('film');
        $new_film = $film_service->createFilm($name, $year, $date, $url);
        return new JsonResponse($new_film);
    }

    /**
     * @Route ("/Film/list", name="listAllFilm")
     */
    public function listFilms()
    {
        $film_service = $this->get('film');
        $allFilms = $film_service->allDataFilms();
        return new JsonResponse($allFilms);
    }

    /**
     * @Route ("/Film/get/{filmId}", name="getFilm")
     */
    public function getFilm($filmId)
    {
        $filmService = $this->get('film');
        $film = $filmService->getFilm($filmId);
        return new JsonResponse($film);
    }

    /**
     * @Route ("/Film/update/{id}/{name}/{year}/{date}/{url}", name="update_film")
     */
    public function updateFilm($id, $name, $year, $date, $url)
    {
        $film_service = $this->get('film');
        $film_updated = $film_service->updateFilm($id, $name, $year, $date, $url);

        $this->response = new Response();
        if($film_updated) {
            $this->sendContentOk(json_encode($film_updated));
        }else{
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        return $this->response;
    }

    /**
     * @Route ("/Film/delete/{id}", name="delete_film")
     */
    public function removeFilm($id)
    {
        $film_service = $this->get('film');
        $responseDeleteFilm = $film_service->deleteFilm($id);

        $this->response = new Response();
        if($responseDeleteFilm) {
            $this->sendContentOk($responseDeleteFilm);
        }else{
            $this->response->setContent('Error: ' . Response::HTTP_NOT_FOUND);
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        return $this->response;
    }

    private function sendContentOk($content)
    {
        $this->response->setContent($content);
        $this->response->setStatusCode(Response::HTTP_OK);
        $this->response->headers->set('content-type', 'text/html');
    }

}