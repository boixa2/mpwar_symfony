<?php

namespace FilmBundle\Service;

use Doctrine\ORM\EntityManager;
use FilmBundle\Entity\Film;
use Symfony\Component\EventDispatcher;

use CacheBundle\CacheEventListener\FilmCacheListener;

/**
 * Date: 08/03/16
 * Time: 21:22
 */
class FilmService
{
    private $entity_manager;
    private $dispatcher;
    private $cacheEventListener;

    public function __construct(EntityManager $entity_manager,
                                EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        $this->entity_manager   = $entity_manager;
        $this->dispatcher       = $eventDispatcher;
    }

    public function createFilm($name, $year, $date, $url)
    {
        $new_film = new Film();
        $new_film->setName($name);
        $new_film->setYear($year);
        $datetime_format = new \DateTime($date);
        $new_film->setDate($datetime_format);
        $new_film->setUrl($url);

        $this->persistFilm($new_film);
        return $new_film->filmToArray();
    }

    public function allDataFilms()
    {
        $this->cacheEventListener = new FilmCacheListener($this->entity_manager,
                                                          $this->dispatcher,
                                                          $this);
        $this->cacheEventListener->dipatchFilmsListListeners();
        $all_films = $this->cacheEventListener->getFilmsListResults();

        $films_array = array();
        foreach($all_films as $film) {
            array_push($films_array, $film->filmToArray());
        }

        return $films_array;
    }

    public function getFilm($filmId)
    {
        $film = $this->entity_manager
            ->getRepository('FilmBundle:Film')
            ->find($filmId);
        return isset($film) ? $film->filmToArray() : array("Error" => "Unseted film with ID: $filmId");
    }

    public function updateFilm($id, $name, $year, $date, $url)
    {
        $film = $this
            ->entity_manager
            ->getRepository('FilmBundle:Film')
            ->find($id);
        $film->setName($name);
        $film->setYear($year);
        $datetime_format = new \DateTime($date);
        $film->setDate($datetime_format);
        $film->setUrl($url);

        $this->persistFilm($film);
        return $film->filmToArray();
    }

    public function deleteFilm($id)
    {
        $film = $this
            ->entity_manager
            ->getRepository('FilmBundle:Film')
            ->find($id);
        if($film == null) return "Impossible to delete film with ID: $id - It not exists in DB";
        $this->entity_manager->remove($film);
        $this->entity_manager->flush($film);
        return "ok";
    }

    private function persistFilm($film)
    {
        $this->entity_manager->persist($film);
        $this->entity_manager->flush($film);
    }

}