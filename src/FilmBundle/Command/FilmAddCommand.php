<?php
/**
 * Date: 11/03/16
 * Time: 00:24
 */

namespace FilmBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class FilmAddCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('film:add')
            ->setDescription('Add new film ~ Params: name, year, date, url')
            ->addArgument('name')
            ->addArgument('year')
            ->addArgument('date')
            ->addArgument('url');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name   = $input->getArgument('name')   ? $input->getArgument('name')   : false;
        $year   = $input->getArgument('year')   ? $input->getArgument('year')   : false;
        $date   = $input->getArgument('date')   ? $input->getArgument('date')   : false;
        $url    = $input->getArgument('url')    ? $input->getArgument('url')    : false;

        $filmService   = $this->getContainer()->get('film');
        $film_added     = $filmService->createFilm($name, $year, $date, $url);

        $output->writeln('OK: ' . json_encode($film_added));
    }

}