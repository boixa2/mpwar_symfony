<?php

// http://symfony.com/doc/current/components/event_dispatcher/introduction.html
// Registering Event Listeners in the Service Container

namespace FilmBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Date: 10/03/16
 * Time: 19:52
 */
class FilmListCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('film:list')
            ->setDescription('List all films recorded. No params are required.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filmService = $this->getContainer()->get('film');
        $filmList = $filmService->allDataFilms();
        $output->writeln(json_encode($filmList));
    }

}