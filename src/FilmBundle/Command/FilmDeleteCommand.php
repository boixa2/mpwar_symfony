<?php
/**
 * Date: 11/03/16
 * Time: 00:25
 */

namespace FilmBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class FilmDeleteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('film:delete')
            ->setDescription('Delete existing film ~ Params: filmId')
            ->addArgument('filmId');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filmId = $input->getArgument('filmId') ? $input->getArgument('filmId') : false;
        $filmService = $this->getContainer()->get('film');
        $deleteResponse = $filmService->deleteFilm($filmId);
        $output->writeln($deleteResponse);
    }

}