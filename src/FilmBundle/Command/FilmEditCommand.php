<?php
/**
 * Created by PhpStorm.
 * User: buixasus
 * Date: 11/03/16
 * Time: 00:25
 */

namespace FilmBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class FilmEditCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('film:update')
            ->setDescription('Update existing film ~ Params: filmId, name, year, date, url')
            ->addArgument('filmId')
            ->addArgument('name')
            ->addArgument('year')
            ->addArgument('date')
            ->addArgument('url');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filmId = $input->getArgument('filmId') ? $input->getArgument('filmId') : false;
        $name   = $input->getArgument('name')   ? $input->getArgument('name')   : false;
        $year   = $input->getArgument('year')   ? $input->getArgument('year')   : false;
        $date   = $input->getArgument('date')   ? $input->getArgument('date')   : false;
        $url    = $input->getArgument('url')    ? $input->getArgument('url')    : false;

        $filmService = $this->getContainer()->get('film');
        $updatedFilm = $filmService->updateFilm($filmId, $name, $year, $date, $url);
        $output->writeln(json_encode($updatedFilm));
    }

}