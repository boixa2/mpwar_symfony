mpwar_symfony
=============

A Symfony project created on March 8, 2016, 7:44 pm.

1. Clone project.
2. Make sure that have been istalled composer, if not try:
git clone git@bitbucket.org:boixa2/mpwar_symfony.git
3. Install project dependencies:
```
#!shell
composer update
```
4. Create DB schema:
```
#!shell
 php bin/console doctrine:database:create
 php bin/console doctrine:schema:create
```
* Film commands:

```
#!shell
film:add
film:edit
film:remove
film:list

```
* Execute this to get more command information:

```
#!shell

php bin/console
```